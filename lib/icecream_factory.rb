class IcecreamFactory
  attr_accessor :refregirators_strategy, :icecreams
  
  def initialize(strategy = TemperatureStrategy.new(self))
    @icecreams = []
    @refregirators_strategy = strategy
  end

  def insert(icecream)
    @icecreams << icecream
    @icecreams.uniq! {|item| item.min.to_s + "&&" + item.max.to_s}
  end
  
  def required_refregirators_count
    @refregirators_strategy.count
  end
end
