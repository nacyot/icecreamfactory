class Icecream
  attr_reader :range    
  
  def self.can_share_refregerator?(icecreams)
    return false if icecreams.length < 2
    union = icecreams.reduce(:&)
    union.null? ? false : true
  end
  
  def initialize(min, max)
    @range = min..max
  end

  def min
    @range.min
  end

  def max
    @range.max
  end

  def &(other)
    range = to_a & other.to_a
    Icecream.new(range.min, range.max)
  end

  def |(other)
    range = to_a | other.to_a
    Icecream.new(range.min, range.max)
  end

  def to_a
    null? ? [] : @range.to_a
  end
  
  def null?
    true if @range.min.nil? and @range.max.nil?
  end
end
