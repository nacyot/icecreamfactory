class CombinationStrategy
  def initialize(factory)
    @factory = factory
  end

  def count
    @icecreams = @factory.icecreams.clone

    combinations = combinations_be_possible
    combinations.empty? ? @icecreams.length : combinations.min{ |combination| combination.length }.length
  end

  def combinations
    combinations = []
    
    2.upto(@icecreams.length).each do |i|
      combinations.concat @icecreams.combination(i).to_a
    end

    combinations
  end

  def combinations_which_can_share
    combinations().delete_if do |combination|
      !(Icecream.can_share_refregerator? combination)
    end
  end

  def combinations_be_possible
    target = combinations_which_can_share
    icecreams_set = @icecreams.to_set
    combinations = []
    
    1.upto(@icecreams.length - 1).each do |i|
      combinations.concat target.combination(i).to_a
    end

    combinations.delete_if do |combination|       
      combination.flatten.to_set != icecreams_set
    end
  end
end
