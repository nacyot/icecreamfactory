require 'spec_helper'

class TemperatureStrategy
  def initialize(factory)
    @factory = factory
  end
  
  def count
    icecreams = @factory.icecreams.clone
    
    ref_num = 0
    
    range = ((icecreams.min_by{|item| item.min}.min)..(icecreams.max_by{|item| item.max}.max)).to_a
    temperatures = Hash[range.zip(Array.new(range.length) { [] })]

    icecreams.each do |icecream|
      icecream.range.to_a.each do |temp|
        temperatures[temp] << icecream
      end
    end

    temperatures = temperatures.sort { |a, b| b[1].length <=> a[1].length }

    temperatures.each do |item|
      delete_num = 0
      item[1].each{ |icecream| delete_num += 1 if icecreams.delete icecream}
      ref_num += 1 if delete_num > 0
      break if icecreams.length == 0
    end
    
    ref_num
  end
end


 
