# -*- coding: utf-8 -*-
require 'spec_helper' 

describe TemperatureStrategy do
  before :each do
    @factory = IcecreamFactory.new
  end

  describe "냉장고 개수 테스트" do
    it "하나의 아이스크림이 있다면 하나의 냉장고만 필요" do
      @factory.insert Icecream.new(-10, -5)
      @factory.required_refregirators_count.should eq(1)
    end

    # 예를들어 유지온도가 각각 -20 oc~ -15 oc, -14 oc~ -5 oc , -18 oc~
    # -13 oc, -5 oc~ -3 oc인 네 개의 아이스크림이 있다고 하면
    it "4개의 아이스크림이 있다면 두 개의 냉장고가 필요" do
      @factory.insert Icecream.new(-20, -15)
      @factory.insert Icecream.new(-14, -5)
      @factory.insert Icecream.new(-18, -13)
      @factory.insert Icecream.new(-5, -3)
      @factory.required_refregirators_count.should eq(2)
    end

    it "냉장고를 같이 쓸 수 있는 2개의 아이스크림이 있다면 한 개의 냉장고가 필요" do
      @factory.insert Icecream.new(-16, -15)
      @factory.insert Icecream.new(-20, -16)
      @factory.required_refregirators_count.should eq(1)
    end

    it "냉장고를 같이 쓸 수 없는 2개의 아이스크림이 있다면 두 개의 냉장고가 필요" do
      @factory.insert Icecream.new(-5, -1)
      @factory.insert Icecream.new(-10, -8)
      @factory.required_refregirators_count.should eq(2)
    end
    
    it "냉장고를 같이 쓸 수 없는 100개의 아이스크림은 100개의 냉장고가 필요" do
      -100.upto(-1).each do |i|
        @factory.insert Icecream.new(i, i)
      end

      @factory.required_refregirators_count.should eq(100)
    end

    it "냉장고를 같이 쓸 수 있는 100개의 아이스크림은 1개의 냉장고가 필요" do
      -100.upto(-1).each do |i|
        @factory.insert Icecream.new(-10, -10)
      end

      @factory.required_refregirators_count.should eq(1)
    end      
  end
end

