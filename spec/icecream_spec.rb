require 'spec_helper'

describe Icecream do
  let(:icecream_nil) { Icecream.new(nil, nil) }
  let(:icecream) { Icecream.new(-10, -5) }
  let(:icecream_two) { Icecream.new(-7, -1) }
  let(:icecream_three) { Icecream.new(-6, -6)}
  let(:icecream_four) { Icecream.new(-25, -20) }
  let(:icecream_and) { icecream & icecream_two }
  let(:icecream_or) { icecream | icecream_two }

  describe "can shore refregerator?" do
    it { Icecream.can_share_refregerator?([icecream]).should be_false }
    it { Icecream.can_share_refregerator?([icecream, icecream_two]).should be_true }
    it { Icecream.can_share_refregerator?([icecream, icecream_two, icecream_three]).should be_true }
    it { Icecream.can_share_refregerator?([icecream, icecream_four]).should be_false }
    it { Icecream.can_share_refregerator?([icecream, icecream_two, icecream_four]).should be_false }
  end
  
  describe "range" do
    it { icecream.range.should be_instance_of(Range) }
    it { icecream.min.should eq(-10) }
    it { icecream.max.should eq(-5) }
  end
  
  describe "null?" do
    it { icecream_nil.null?.should be_true }
    it { icecream_nil.min.should be_nil }
    it { icecream_nil.max.should be_nil }
  end

  describe "&" do
    it { icecream_and.should be_instance_of(Icecream) }
    it { icecream_and.min.should eq(-7) }
    it { icecream_and.max.should eq(-5) }
  end

  describe "|" do
    it { icecream_or.should be_instance_of(Icecream) }
    it { icecream_or.min.should eq(-10) }
    it { icecream_or.max.should eq(-1) }
  end

end

